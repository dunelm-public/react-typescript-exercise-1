# Frontend Exercise - React TypeScript

- React 18.2
- Typescript 4.6
- Node 16

## Getting started

Run `yarn` to install dependencies and `yarn dev` to spin up development server

```
  yarn
  yarn dev
```

## Tests

The app has been configured with [jest](https://jestjs.io/) and [testing-library](https://testing-library.com/docs/)

Tests can be found in the `__tests__` folder

Please write tests to the extent necessary to drive your development

A few existing test

## Types

The file `types.ts` contains a `Product` interface.

## API

The file `api.ts` includes a virtual API with an async `data` function that returns a list of products loaded from a `products.json` file

## App

The main `App` component displays

- `HeaderBar` 
- `Basket`
- `Products`

## Exercise 1: Load and display products

Use the hooks `useEffect` and `useState` to load products and display them in a list
For now simply display the `name` of each product

## Exercise 1.1: Product component

Create a separate `Product` component that you use to display each product.

Display the following product properties

- Name: `Sofa`
- Description
- Availability: `Available` or `Unavailable`
- Price: `$ 5.00`

To display the html of `description` you may use [dangerouslysetinnerhtml](https://reactjs.org/docs/dom-elements.html#dangerouslysetinnerhtml)

- Write tests for the `Product` component 

## Exercise 1.2: Style Products

Add Responsive Styling to Products so that you display 4 products per row for devices with more than 1000 pixels.

## Exercise 2: Filter and sort products

The user should be able to apply multiple filters to narrow down
the products displayed.

### Exercise 2.1 : Filter by inStock availability

Add a filter to display only products where `inStock` is `true`

Write a test that confirms that no products out of stock are displayed. 

### Exercise 2.2 : Filter by price

Add a filter `button` named `Cheap products` which filters by price (<= 10).

Write a test for the cheap filter which simulates a user clicking on the button. 
The test should verify that only cheap products are displayed.

### Exercise 2.3 : Sort by price

Add functionality to ensure filtered products are always sorted by price (cheapest first).

### Exercise 2.4 : Filter products by name

Add an `input` search box to allow filtering products by `name` matching (`includes` or regex).

## Exercise 3 : Shopping basket

Let user select filtered products which are added to shopping basket. 
You may do this by adding a `Buy` button to the `Product` component.

Display the products in shopping basket

- Name
- Price

Display total price of basket under the basket items

Write a test which:
- adds two items to the shopping basket verifies
- verifies they products are displayed in the basket
- verifies the total basket price

## Exercise 4 : Display number of basket items in navigation bar

Display number of basket items in the navigation bar 

Write App test which verifies that basket items counter is increased when a basket item is added.
